/* -*- Mode: C; -*- */
/* Creator: Bronis R. de Supinski (bronis@llnl.gov) Fri Mar  17 2000 */
/* no-error.c -- do some MPI calls without any errors */
/* Adapted to X10: Tiago Cogumbreiro (cogumbreiro@users.sf.net),
                   Francisco Martins (fmartins@di.fc.ul.pt) */
import x10.array.*;

public class WaitDeadlock {
    public static def main (args:Array[String]) {
      val nprocs = 2;
      val buf_size = 128;
      val rank0 = Clock.make();
      val rank1 = Clock.make();
      val MPI_Barrier = Clock.make();
      /* init */
      /*
      MPI_Init (&argc, &argv);
      MPI_Comm_size (MPI_COMM_WORLD, &nprocs);
      MPI_Comm_rank (MPI_COMM_WORLD, &rank);
      MPI_Get_processor_name (processor_name, &namelen);
      printf ("(%d) is alive on %s\n", rank, processor_name);
      fflush (stdout);
	    int i;
	    */
	  // if (rank == 0)
      async clocked(MPI_Barrier, rank0, rank1) {
          // MPI_Barrier (MPI_COMM_WORLD);
          MPI_Barrier.advance();
          //memset (buf0, 0, buf_size);

          //MPI_Irecv (buf1, buf_size, MPI_INT, 1, 0, MPI_COMM_WORLD, &req);
          rank0.resume();

          //MPI_Wait (&req, &status);
          rank0.advance();

          //MPI_Send (buf0, buf_size, MPI_INT, 1, 0, MPI_COMM_WORLD);
          rank1.advance();
          MPI_Barrier.advance();
      }
      // if (rank == 1)
      async clocked(MPI_Barrier, rank0, rank1) {
          // MPI_Barrier (MPI_COMM_WORLD);
          MPI_Barrier.advance();
          
          //memset (buf1, 1, buf_size);

          //MPI_Irecv (buf0, buf_size, MPI_INT, 0, 0, MPI_COMM_WORLD, &req);
          rank1.resume();

          //MPI_Wait (&req, &status);
          rank1.advance();

          //MPI_Send (buf1, buf_size, MPI_INT, 0, 0, MPI_COMM_WORLD);
          rank0.advance();
          
          // MPI_Barrier (MPI_COMM_WORLD);
          MPI_Barrier.advance();
      }
      //MPI_Finalize ();
      //printf ("(%d) Finished normally\n", rank);
    }
}
/* EOF */
