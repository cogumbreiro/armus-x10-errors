# Armus-X10 tests

This project uses X10 to encode the synchronisation patterns listed in
[ISP Tests](http://www.cs.utah.edu/formal_verification/ISP_Tests/) to exercise the correctness of the [Armus-X10](https://bitbucket.org/cogumbreiro/armus-x10) tool.

| # | Test Name (.c) | Test Name (.x10) | ISP  | Armus-X10 |
|---|----------------| ---------------- | ---- | ----------|
| 1 | [any_src-can-deadlock.c](http://www.cs.utah.edu/formal_verification/ISP_Tests/Umpire_Tests/any_src-can-deadlock.c) |  [AnySrcCanDeadlock.x10](https://bitbucket.org/cogumbreiro/armus-x10-errors/src/master/AnySrcCanDeadlock.x10) | No Deadlock Detected 2 Interleavings | Detected |
| 20 | [basic-deadlock.c](http://www.cs.utah.edu/formal_verification/ISP_Tests/Umpire_Tests/basic-deadlock.c) | [BasicDeadlock.x10](https://bitbucket.org/cogumbreiro/armus-x10-errors/src/master/BasicDeadlock.x10) | Deadlock Detected 1 Interleaving | Detected |
| 70 | [wait-deadlock.c](http://www.cs.utah.edu/formal_verification/ISP_Tests/Umpire_Tests/wait-deadlock.c) | [WaitDeadlock.x10](https://bitbucket.org/cogumbreiro/armus-x10-errors/src/master/WaitDeadlock.x10) | Deadlock Detected 1 Interleaving | Detected |

