import x10.util.concurrent.*;
public class MPI {
    public static stdout = Console.OUT;
    public static type Communicator = Clock;
    public static type MPIType = Empty;
    public static type MPI_Request = Empty;
    public static val MPI_ANY_SOURCE = -1;
    public static val MPI_INT = new Empty();
    public static class Rank(id:Long, clock:Clock) {
        public def this(id:Long) {
            property(id, Clock.make());
        }
        val buffer:Rail[AtomicInteger] = new Rail[AtomicInteger](2, (x:Long) => new AtomicInteger(0n));
        public def ticket():AtomicInteger {
            return buffer(clock.phase() % 2);
        }
        public def nextTicket():AtomicInteger {
            return buffer((clock.phase() + 1) % 2);
        }
        public def MPI_Send(src:Long) {
            while (true) {
                val currentTicket = ticket().getAndIncrement();
                clock.advance();
                if (currentTicket == 0n) {
                    //Console.OUT.println(src + ": passed");
                    break;
                }
                // try again
                //Console.OUT.println(src + ": trying again: " + currentTicket);
            }
        }
        public def MPI_Recv():void {
            nextTicket().set(0n);
            clock.advance();
        }
        public def MPI_Irecv():MPI_Request {
            nextTicket().set(0n);
            clock.resume();
            return new Empty();
        }
        public def drop() {
            clock.drop();
        }
    }
    
    public val MPI_COMM_WORLD:Communicator;
    private val ranks:Rail[Rank];
    private val currentRank:Long;
    
    public def this(participants:Long) {
        this.MPI_COMM_WORLD = Clock.make();
        this.ranks = new Rail[Rank](participants, (x:Long) => new Rank(x));
        this.currentRank = -1;
    }
    public def this(m:MPI, currentRank:Long) {
        this.MPI_COMM_WORLD = m.MPI_COMM_WORLD;
        this.ranks = m.ranks;
        this.currentRank = currentRank;
    }
    public def this(MPI_COMM_WORLD:Communicator, ranks:Rail[Rank], currentRank:Long) {
        this.MPI_COMM_WORLD = MPI_COMM_WORLD;
        this.ranks = ranks;
        this.currentRank = currentRank;
    }
    
    public def MPI_Barrier(comm:Communicator) {
        comm.advance();
    }
    
    public def MPI_Send(buf:Any, buf_size:Long, mpiType:MPIType, rank:Long, tag:Long, com:Communicator) {
        //Console.OUT.printf("%d: MPI_Send(%d)\n", currentRank, rank);
        ranks(rank).MPI_Send(currentRank);
    }

    public def MPI_Irecv(buf:Any, buf_size:Long, mpiType:MPIType, rank:Long, tag:Long, comm:Communicator):MPI_Request {
        return ranks(currentRank).MPI_Irecv();
    }
                
    public def MPI_Recv(buf:Any, buf_size:Long, mpiType:MPIType, rank:Long, tag:Long, comm:Communicator) {
        ranks(currentRank).MPI_Recv();
    }
    
    public def MPI_Wait(req:MPI_Request) {
        // ?
    }
    
    public MPI_Comm_rank (comm:Communicator)  {
        return currentRank;
    }
    
    public def MPI_Finalize () {}
    
    public def MPI_Init() {}
    
    public def MPI_Get_processor_name() {
        return Runtime.getName();
    }
    
    public def rank(rank:Long):Clock {
        return ranks(rank).clock;
    }
    
    public def MPI_Comm_size (comm:Communicator) {
        return size();
    }
    
    public def size() {
        return ranks.size;
    }
    
    public def printf(fmt:String, obj:Any) {
        Console.OUT.printf(fmt, obj);
    }
    
    public def printf(fmt:String, obj1:Any, obj2:Any) {
        Console.OUT.printf(fmt, obj1, obj2);
    }
    
    public def printf(fmt:String) {
        Console.OUT.printf(fmt);
    }
    
    public def fflush(out:x10.io.Writer) {
        out.flush();
    }
    
    public static def memset[T](buffer:Rail[T], value:T) {
        for (p in buffer.range) {
            buffer(p) = value;
        }
    }
    
    public static def memset[T](buffer:Rail[T], value:T, size:Long) {
        var counter:Long = 0;
        for (p in buffer.range) {
            counter++;
            if (counter >= size) {
                break;
            }
            buffer(p) = value;
        }
    }
    
    public static def sleep(millis:Long) {
        System.sleep(millis);
    }
}
