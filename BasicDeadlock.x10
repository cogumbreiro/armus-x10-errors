/* -*- Mode: C; -*- */
/* Creator: Bronis R. de Supinski (bronis@llnl.gov) Fri Mar  17 2000 */
/* no-error.c -- do some MPI calls without any errors */
/*
/* Adapted to X10: Tiago Cogumbreiro (cogumbreiro@users.sf.net),
                   Francisco Martins (fmartins@di.fc.ul.pt) */
/*
#include <stdio.h>
#include "mpi.h"

#define buf_size 128
*/
public class BasicDeadlock {
    public static def main (Array[String]) {
        /*
        int nprocs = -1;
        int rank = -1;
        */
        val processor_name = "proof.cs.utah.edu";
        /*
        int namelen = 128;
        int buf0[buf_size];
        int buf1[buf_size];
        MPI_Status status;
        */

        /* init */
        /*
        MPI_Init (&argc, &argv);
        MPI_Comm_size (MPI_COMM_WORLD, &nprocs);
        MPI_Comm_rank (MPI_COMM_WORLD, &rank);
        MPI_Get_processor_name (processor_name, &namelen);
        */
        val MPI_Barrier = Clock.make();
        val rank0 = Clock.make();
        val rank1 = Clock.make();
        
        // if (rank == 0)
        async clocked(MPI_Barrier, rank0, rank1) {
            val rank = 0;
            //printf ("(%d) is alive on %s\n", rank, processor_name);
            //fflush (stdout);
            Console.OUT.print("(%d) is alive on %s\n", rank, processor_name);
            //MPI_Barrier (MPI_COMM_WORLD);
            MPI_Barrier.advance();
            
            //memset (buf0, 0, buf_size);

            //MPI_Recv (buf1, buf_size, MPI_INT, 1, 0, MPI_COMM_WORLD, &status);
            rank0.advance();

            //MPI_Send (buf0, buf_size, MPI_INT, 1, 0, MPI_COMM_WORLD);
            rank1.advance();
            //MPI_Barrier (MPI_COMM_WORLD);
            MPI_Barrier.advance();

            //MPI_Finalize ();
            //printf ("(%d) Finished normally\n", rank);
            Console.OUT.print("(%d) Finished normally\n", rank);
        }
        
        // if (rank == 1)
        async clocked(MPI_Barrier, rank0, rank1) {
            val rank = 1;
            //printf ("(%d) is alive on %s\n", rank, processor_name);
            //fflush (stdout);
            Console.OUT.print("(%d) is alive on %s\n", rank, processor_name);
            //MPI_Barrier (MPI_COMM_WORLD);
            MPI_Barrier.advance();
            
            //memset (buf1, 1, buf_size);

            //MPI_Recv (buf0, buf_size, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
            rank1.advance();

            //MPI_Send (buf1, buf_size, MPI_INT, 0, 0, MPI_COMM_WORLD);
            rank0.advance();
            
            //MPI_Barrier (MPI_COMM_WORLD);
            MPI_Barrier.advance();

            //MPI_Finalize ();
            //printf ("(%d) Finished normally\n", rank);
            Console.OUT.print("(%d) Finished normally\n", rank);
        }        
    }
}
/* EOF */
