/* -*- Mode: C; -*- */
/* Creator: Bronis R. de Supinski (bronis@llnl.gov) Thu Jan 3 2002 */
/* any_src-can-deadlock.c -- deadlock occurs if task 0 receives */
/*                           from task 2 first; the likely outcome */
/*                           because we sleep task 1 */
/* Adapted to X10: Tiago Cogumbreiro (cogumbreiro@users.sf.net) */
import x10.util.concurrent.*;

public class AnySrcCanDeadlock2(slowRank1:Boolean, slowRank2:Boolean) extends MPI {
    static val buf_size:Long = 128;
    private static def slowdown() {
        System.sleep(300);
    }
    public def this(participants:Long, slowRank1:Boolean, slowRank2:Boolean) {
        super(participants);
        property(slowRank1, slowRank2);
    }
    public def this(m:AnySrcCanDeadlock2, currentRank:Long) {
        super(m, currentRank);
        property(m.slowRank1, m.slowRank2);
    }
    public def run() {
        for (rank in 0..(size()-1)) {
            val mpi = new AnySrcCanDeadlock2(this, rank);
            if (rank == 0) {
                async clocked(MPI_COMM_WORLD, rank(0), rank(1)) {
                    mpi.main();
                }
            } else if (rank == 1) {
                async clocked(MPI_COMM_WORLD, rank(0), rank(1)) {
                    mpi.main();
                }
            } else if (rank == 2) {
                async clocked(MPI_COMM_WORLD, rank(0)) {
                    mpi.main();
                }
            }
            
        }
    }
    public def main() {
        val buf0 = new Rail[Long](buf_size);
        val buf1 = new Rail[Long](buf_size);
        //MPI_Status status;

        /* init */
        MPI_Init ();
        val nprocs = MPI_Comm_size (MPI_COMM_WORLD);
        val rank = MPI_Comm_rank (MPI_COMM_WORLD);
        val processor_name = MPI_Get_processor_name ();
        printf ("(%d) is alive on %s\n", rank, processor_name);
        fflush (stdout);

        MPI_Barrier (MPI_COMM_WORLD);

  if (nprocs < 3)
    {
      printf ("not enough tasks\n");
    }
  else if (rank == 0)
    {
      MPI_Recv (buf1, buf_size, MPI_INT, 
                MPI_ANY_SOURCE, 0, MPI_COMM_WORLD);

      MPI_Send (buf1, buf_size, MPI_INT, 1, 0, MPI_COMM_WORLD);

      MPI_Recv (buf0, buf_size, MPI_INT, 
                MPI_ANY_SOURCE, 0, MPI_COMM_WORLD);
    }
  else if (rank == 1)
    {
      memset (buf0, 0, buf_size);
      
      sleep (60);
      
      MPI_Send (buf0, buf_size, MPI_INT, 0, 0, MPI_COMM_WORLD);

      MPI_Recv (buf1, buf_size, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }
  else if (rank == 2)
    {
      memset (buf1, 1, buf_size);

      MPI_Send (buf1, buf_size, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }

        MPI_Barrier (MPI_COMM_WORLD);

        MPI_Finalize ();
        printf ("(%d) Finished normally\n", rank);
    }

    public static def main(args:Rail[String]) {
        val slowRank1 = args.size == 1 && args(0).equals("1");
        val slowRank2 = args.size == 1 && args(0).equals("2");
        if (args.size > 0 && !args(0).equals("1") && !args(0).equals("2")) {
            Console.ERR.println("Usage: x10 AnySrcCanDeadlock {1,2}");
            Console.ERR.println("\t1\tdelay rank 1 (deadlocks)");
            Console.ERR.println("\t2\tdelay rank 2 (no deadlock)");
            return;
        }
        new AnySrcCanDeadlock2(3, slowRank1, slowRank2).run();
    }
}
/* EOF */
